
# Font Configurations for RichStyle Library

Sharp and clear font settings for:

* Ubuntu font family.
* Liberation fonts.
* MS core fonts.

![Two instances of a text editor, before and after installing `fontconig-config-richstyle`](fontconig-config-richstyle.png "Font configurations for RichStyle library")

It also sets font aliases like this:

* Sans: refers to `Ubuntu` font.
* Serif: refers to `Georgia` font.
* Monospace: refers to `Ubuntu Mono` font.
* Cursive and Fantasy: refer to `Elegante` font.
* `Arial`: refers to `Ubuntu` font.
* `Traditional Arabic`: refers to `Amiri` font.

## Known Issues

* Recently, doesn't work properly; it removes font smoothing and hinting everywhere; i.e. from all the fonts, not only the selected ones!

## Home Page

[https://richstyle.org](https://richstyle.org)
